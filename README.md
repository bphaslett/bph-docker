This is a bash script for running and/or building my docker containers.    
I've merged in some of my bash functions in the hopes that maybe someone will find them useful.  I realize some people use the Dockerfile to build things, but for me this way was easier than learning the syntax, and it was an excuse to practice scripting.  It takes care of building the tree (which you can actually chroot into).  My Dockerfile is usually just this:    
`FROM scratch`    
`MAINTAINER YOUR-NAME-GOES-HERE <email-address@somewhere.com>`    
`SHELL [ "/bin/bash" ]`    
`ADD [ ".", "/" ]`    
    
then the actual containers get built with something like this:    
`docker build -t repository-name .`    
