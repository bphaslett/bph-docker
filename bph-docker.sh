#!/bin/bash

PS3="Please enter a selection: "

# make bare bones skeleton container
container_skel() {
  local u
  printf "This will create a bare bones docker container.\n"
  printf "Make sure you are in the directory you want to build a container in.\n"
  printf "Press enter to continue, ctrl-c to cancel. "
  read input

  mkdir -p {bin,sbin,var,lib64,lib/x86_64-linux-gnu,var/run,var/logs,usr/lib/x86_64-linux-gnu,usr/bin,usr/sbin,usr/share,etc/bash,etc/ld.so.conf.d,tmp}
  cp -a /sbin/{ifconfig,route,ip} sbin/
  ln -s var/run run
  cp -a /usr/local/lib/libnmalloc.so lib/x86_64-linux-gnu/
#  cp -a /lib/ld-*.so* lib/
#  cp -a /lib32/ld-*.so* lib32/
  cp -a /lib64/ld-*.so* lib64/
  cp -a -t lib/x86_64-linux-gnu /lib/x86_64-linux-gnu/{libnss_*.so,liblzma.so.*,libpthread.so*,libidn.so*,libnsl.so*,libc.so*,libcap.so*,libdl.so*,libresolv.so*,libprocps.so*,librt.so*,libnsl*.so,libtinfo.so*,libcrypt.so*,libcrypt-*.so,ld-*.so,libpthread-*.so,libpcre.so*,libm.so*,libselinux.so*,libgcrypt.so*,libgpg-error.so*,libmnl.so*,libsystemd.so*,libz.so*,libdl-*.so,libc-*.so,libresolv-*.so,librt-*.so,libm-*.so}
  cp -at bin/ /bin/{bash,ip,dash,ping,sh,false}
  cp -a -t usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/{libelf.so*,libidn2.so*,liblz4.so*,libunistring.so*,libnettle.so*}
  cp -a /usr/bin/groups usr/bin/
  cp -ar /etc/ld.so.conf.d etc/
  cp -art etc/ /etc/{bash.bashrc,bash/code,passwd,.pwd.lock,group,profile,ld.so.cache,nsswitch.conf,ld.so.conf} 

  # sanitize a few things
  declare -a users=($(ls -1d /home/* | sed 's,.*/,,g'))
  for u in $(echo ${users[*]}); do
    sed -r ':next N; $!b next; s,\n'"$u"'[^\n]*(\n|$),,g' -i etc/passwd
    sed -r ':next N; $!b next; s,\n'"$u"'[^\n]*(\n|$),,g' -i etc/group
    sed 's/,'"$u"'//g' -i etc/group
    sed 's/'"$u"'//g' -i etc/group
  done
  sed ':next N; $!b next; s/#CUSTOM_BEGIN.*CUSTOM_END//g' -i etc/bash.bashrc
}

# determine whether something is an ELF binary
is_elf() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${FUNCNAME[0]} [file to check]"
    return
  fi
  local efile="$1"
  [[ -n $(file "$efile" | grep ELF| grep LSB | egrep  '(executable|shared)') ]] && echo "$efile"
}

# find ELF binaries in a directory
find_elf() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${FUNCNAME[0]} [dir to search]"
    return
  fi
  local edir="$1"
  for fn in $(find "$edir" -type f); do is_elf "$fn"; done
}

# determine libraries used by a program (simple)
_getlibs() {
  if [[ $# -ne 1 ]]; then
    echo "Usage: ${FUNCNAME[0]} [file name]"
    return
  fi
  local file="$1"
  ldd "$file" | awk '{print $3}'|grep "\.so"
}
# determine libraries used by a program (recursive)
getlibs() {
  if [[ $# -ne 2 ]]; then
    echo "Usage: ${FUNCNAME[0]} [iterations] [file name]"
    return
  fi
  local its="$1"
  local file="$2"
  if [[ "$its" -le 1 ]]; then

    _getlibs "$file"

  elif [[ "$its" -eq 2 ]]; then
    local lib

    for lib in $(_getlibs "$file"); do
      echo "$lib"
      _getlibs "$lib"
    done | sort | uniq

  elif [[ "$its" -eq 3 ]]; then
    local lib
    local liblib

    for lib in $(_getlibs "$file"); do
      echo "$lib"
      _getlibs "$lib"
      for liblib in $(_getlibs "$lib"); do
        _getlibs "$liblib"
      done
    done | sort | uniq

  elif [[ "$its" -eq 4 ]]; then
    local lib
    local liblib
    local libliblib

    for lib in $(_getlibs "$file"); do
      echo "$lib"
      _getlibs "$lib"
      for liblib in $(_getlibs "$lib"); do
        _getlibs "$liblib"
        for libliblib in $(_getlibs "$liblib"); do
          _getlibs "$libliblib"
        done
      done
    done | sort | uniq

  fi
}
# determine libraries used by a several binaries (recursive)
getlibs_multibin() {
  local i
  if [[ $# -lt 1 ]]; then
    echo "Usage: ${FUNCNAME[0]} [binary 1] [binary 2] ..."
    return
  fi
  declare bins=($@)

  for i in $(echo ${bins[*]}); do
    getlibs 2 "$i"
  done | sort | uniq
}
# determine libraries used by all binaries in a tree (recursive)
getlibs_dir() {
  if [[ $# -lt 1 ]]; then
    echo "Usage: ${FUNCNAME[0]} [directory] ..."
    return
  fi
  local dir="$1"
  getlibs_multibin $(find_elf ${dir} | tr '\n' " ")
}
# list missing libraries used by binaries in a tree (recursive)
missing_libs() {
  local i
  getlibs_dir . > deps.txt
  for i in $(cat deps.txt | sed 's,.*/,,g'); do
    find -name "$i" | grep -E '*' || echo "missing $i"
  done | grep "missing "
}

cleanup() {
  rm missing.txt missing-full.txt deps.txt 2> /dev/null
  echo ""
  exit
}

# list and/or install missing libraries used by binaries in a tree (recursive)
install_missing_libs() {
  local i
  printf "This will install missing libraries into the current working directory.\n"
  printf "Make sure you are in the directory you want to build a container in.\n"
  printf "Press enter to continue, ctrl-c to cancel. "
  read input
  printf "Searching..\n"

  missing_libs > missing.txt
  printf "The following libraries are missing: \n"
  cat missing.txt | awk '{print $2}'
  trap cleanup SIGINT
  printf "Press enter to continue, ctrl-c to cancel. "
  read input
  printf "Installing to %s..\n" $(pwd)

  for i in $(cat missing.txt | awk '{print $2}'); do
    grep "$i" deps.txt;
  done > missing-full.txt
  for i in $(cat missing-full.txt  | sed -ne '\,^/lib, {
    s,.*/,,p
    s,\.so.*,\.so,p
  }'); do
    cp -a /lib/x86_64-linux-gnu/"$i"* lib/x86_64-linux-gnu/;
  done
  for i in $(cat missing-full.txt  | sed -ne '\,^/usr/lib, {
    s,.*/,,p
    s,\.so.*,\.so,p
  }'); do
    cp -a /usr/lib/x86_64-linux-gnu/"$i"* usr/lib/x86_64-linux-gnu/;
  done

  cleanup
}

build_cap_list(){
    local match
    local cap
    local count=0
    declare -a need_caps
    declare -a cap_list
    declare -a all_caps=(chown dac_override dac_read_search fowner fsetid kill setgid setuid setpcap linux_immutable net_bind_service net_broadcast net_admin net_raw ipc_lock ipc_owner sys_module sys_rawio sys_chroot sys_ptrace sys_pacct sys_admin sys_boot sys_nice sys_resource sys_time sys_tty_config mknod lease audit_write audit_control setfcap mac_override mac_admin syslog wake_alarm block_suspend audit_read)

    printf "**********************************************\n"
    printf "* cap_list is a list of capabilities to drop *\n"
    printf "**********************************************\n\n"
    printf "This is a list of all capabilities supported:\n\n"
    printf "${all_caps[*]}\n\n"
    printf "Enter a whitespace separated list of capabilities you need: "
    read -a need_caps

    for cap_name in $(echo ${all_caps[*]}); do
      for cap in $(echo ${need_caps[*]}); do
        if [[ "$cap_name" == "$cap" ]]; then
          match=1
        fi
      done
      if [[ $match -eq 0 ]]; then
        cap_list[$count]=$cap_name
        ((count++))
      fi
      match=0
    done

    echo "declare -a cap_list=(${cap_list[*]})"
}

remove_old() {
  local container=$1
  if [[ -n $(docker ps -a | grep $container) ]]; then
    echo "The $container container is already running; stopping the old one."
    docker stop ${container}
    if [[ -n $(docker ps -a | grep $container) ]]; then
      echo "Removing the old container."
      docker rm ${container}
    fi
  fi
}

menu() {
  local sel
  local main_cmd
  local container
  local cap_name
  local cap_cmd
  local dev_mnt
  local dev_cmd

  printf "===================\n"
  printf "Brian's Docker Menu\n"
  printf "===================\n\n"
  select sel in \
  "run stubby container" \
  "run unbound container" \
  "run irssi container" \
  "build bare-bones skeleton container" \
  "install missing libs" \
  "build cap_list" \
  "quit"; do
    case "$REPLY" in
      1)
        container="stubby"

        declare -a cap_list=(chown dac_override dac_read_search fowner fsetid kill setgid setuid setpcap linux_immutable net_broadcast net_admin net_raw ipc_lock ipc_owner sys_module sys_rawio sys_chroot sys_ptrace sys_pacct sys_admin sys_boot sys_nice sys_resource sys_time sys_tty_config mknod lease audit_write audit_control setfcap mac_override mac_admin syslog wake_alarm block_suspend audit_read)
        declare -a dev_mounts=(random urandom null)

        for cap_name in $(echo ${cap_list[*]}); do
          cap_cmd="${cap_cmd} --cap-drop=${cap_name}"
        done
        for dev_mnt in $(echo ${dev_mounts[*]}); do
          dev_cmd="${dev_cmd} --device=/dev/$dev_mnt"
        done

        main_cmd="exec docker run --name ${container} ${cap_cmd} ${dev_cmd} --read-only --restart=always --detach=true"
        main_cmd=" ${main_cmd} --publish 127.0.0.1:53:53/udp --publish 127.0.0.1:53:53/tcp straelyn/${container} stubby.sh"

        remove_old $container
        ${main_cmd}
        ;;
      2)
        container="unbound"
        declare -a dev_mounts=(random urandom null)
        declare -a cap_list=(audit_control audit_read audit_write block_suspend dac_override dac_read_search ipc_lock ipc_owner kill lease linux_immutable mac_admin mac_override mknod net_admin net_broadcast net_raw setfcap setpcap sys_admin sys_boot syslog sys_module sys_nice sys_pacct sys_ptrace sys_rawio sys_time sys_tty_config wake_alarm)

        for cap_name in $(echo ${cap_list[*]}); do
          cap_cmd="${cap_cmd} --cap-drop=${cap_name}"
        done
        for dev_mnt in $(echo ${dev_mounts[*]}); do
          dev_cmd="${dev_cmd} --device=/dev/$dev_mnt"
        done

        main_cmd="docker run --name ${container} ${cap_cmd} ${dev_cmd} --read-only --restart=always --detach=true"
        main_cmd="${main_cmd} --mount type=tmpfs,tmpfs-mode=0755,tmpfs-size=5M,destination=/var/lib/unbound"
        main_cmd="${main_cmd} --publish 127.0.0.1:53:53/udp --publish 127.0.0.1:53:53/tcp straelyn/${container} unbound.sh"

        remove_old $container
        ${main_cmd}
        ;;
      3)
        container="irssi"
        declare -a dev_mounts=(random urandom null)
        declare -a cap_list=(chown dac_override dac_read_search fowner fsetid kill setgid setuid setpcap linux_immutable net_bind_service net_broadcast net_admin net_raw ipc_lock ipc_owner sys_module sys_rawio sys_chroot sys_ptrace sys_pacct sys_admin sys_boot sys_nice sys_resource sys_time sys_tty_config mknod lease audit_write audit_control setfcap mac_override mac_admin syslog wake_alarm block_suspend audit_read)

        for cap_name in $(echo ${cap_list[*]}); do
          cap_cmd="${cap_cmd} --cap-drop=${cap_name}"
        done
        for dev_mnt in $(echo ${dev_mounts[*]}); do
          dev_cmd="${dev_cmd} --device=/dev/$dev_mnt"
        done

        main_cmd="docker run --name ${container} ${cap_cmd} ${dev_cmd} --read-only --rm --interactive --tty"
        main_cmd="${main_cmd} -v /etc/passwd:/etc/passwd:ro -v /etc/group:/etc/group:ro -v /etc/localtime:/etc/localtime:ro"
        main_cmd="${main_cmd} -e TERM -u $(id -u):$(id -g) -v $HOME:$HOME:shared"
        main_cmd="${main_cmd} --network=host straelyn/${container} irssi.sh"

        remove_old $container
        ${main_cmd}
        ;;
      4)
        container_skel
        ;;
      5)
        install_missing_libs
        ;;
      6)
        build_cap_list
        ;;
      7)
        exit
        ;;
      *)
        printf "\nERROR, INVALID SELECTION\n\n"
        menu
        ;;
    esac
    break
  done

}


main() {
  menu
}

main

